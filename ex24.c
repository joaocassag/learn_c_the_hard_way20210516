#include <stdio.h>
#include <stdlib.h>

#define MAX_DATA 100

typedef enum EyeColot{
  BLUE_EYES, GREEN_EYES, BROWM_EYES,
  BLACK_EYES, OTHER_EYES
} EyeColor;

const char *EYE_COLOR_NAMES[] = {
  "Blue","Green","Brown","Black","Other"
};

typedef struct Person{
  int age;
  char first_name[MAX_DATA];
  char last_name[MAX_DATA];
  EyeColor eyes;
  float income;
} Person;

int main(int argc, char *argv[]){
  Person you = {.age = 0};
  int i = 0;
  char * in = NULL;

  printf("What's your First Name? ");
  in = fgets(you.first_name, MAX_DATA-1, stdin);
  if(!in){
    printf("Failed to read");
    exit(1);
  }

  printf("What's your Last Name? ");
  in = fgets(you.last_name, MAX_DATA-1, stdin);
  if(!in){
    printf("Failed to read");
    exit(1);
  }

  printf("How old are you? ");
  int rc = fscanf(stdin, "%d",&you.age);
  if(!rc && rc<=0){
    printf("Failed to read");
    exit(1);
  }

  printf("What color are your eyes? \n");
  for (i = 0; i <= OTHER_EYES; i++) {
    printf("%d) %s\n",i+1,EYE_COLOR_NAMES[i]);
  }
  printf("\n");
  int eyes = -1;
  rc = fscanf(stdin, "%d",&eyes);
  if(!rc && rc<=0){
    printf("Failed to read");
    exit(1);
  }
  you.eyes = eyes -1;

  printf("---------RESULTS---------\n");
  printf("First Name: %s\n", you.first_name);
  printf("Last Name: %s\n", you.last_name);
  printf("Age: %d\n", you.age);
  printf("Eyes: %s\n", EYE_COLOR_NAMES[you.eyes]);

  return 0;
}
