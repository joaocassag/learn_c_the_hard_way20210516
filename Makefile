CFLAGS=-Wall -g

all: ex1 ex3 ex4 ex6 ex7 ex8 ex9 ex10 ex16 ex17 ex18 ex19 ex20 ex22 ex23 ex24 ex25 ex29 ex31

ex19: object.o

ex22: ex22_main.c ex22.c

# cc -c libex29.c -o libex29.o
# cc -shared -o libex29.so libex29.o
# cc -Wall -g -DNDEBUG ex29.c -ldl -o ex29
#libex29: -c libex29.c -o libex29.o
#libex29: -shared -o libex29.so libex29.o
#ex29: libex29 -DNDEBUG ex29.c -ldl -o ex29

clean:
	rm -rf *.dSYM
	rm -rf *.o
	rm -rf *.so
	rm -f ex1
	rm -f ex3
	rm -f ex4
	rm -f ex6
	rm -f ex7
	rm -f ex8
	rm -f ex9
	rm -f ex10
	rm -f ex16
	rm -f ex17
	rm -f ex18
	rm -f ex19
	rm -f ex20
	rm -f ex22
	rm -f ex23
	rm -f ex24
	rm -f ex25
	rm -f ex29
	rm -f ex31
