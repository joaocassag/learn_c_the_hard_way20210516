#include "ex22.h"
//#include "dbg.h"
#include <stdio.h>
#include <stdlib.h>

const char *MY_NAME = "Joao C. Cassa";

void scop_demo(int count){
  printf("count is: %d\n",count);

  if(count>10){
    int count = 100; //BUG!
    printf("count in this scope is: %d\n",count);
  }

  count = 300;
  printf("count after assign is: %d\n",count);
}

int main(int argc, char *argv[]){
  printf("My name is: %s, age: %d\n",MY_NAME, get_age());

  set_age(100);
  printf("My age is now: %d\n",get_age());

  printf("THE_SIZE is: %d\n",THE_SIZE);
  print_size();

  THE_SIZE = 9;
  printf("THE_SIZE is now: %d\n",THE_SIZE);

  printf("Ratio at first: %f\n",update_ratio(2.0));
  printf("Ratio again: %f\n",update_ratio(10.0));
  printf("Ratio once more: %f\n",update_ratio(300.0));

  int count = 4;
  scop_demo(count);
  scop_demo(count*20);

  printf("count afte calling scop_demo: %d:\n",count);

  return 0;
}
