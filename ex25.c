#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#define MAX_DATA 100

int read_string(char ** out_string, int max_buffer){
  *out_string = calloc(1,max_buffer+1);
/*
  if(!out_string){
    printf("Failed to allocate memory");
    exit(1);
  }*/

  char *result = fgets(*out_string, max_buffer, stdin);
  if(!result){
    printf("Failed to allocate memory");

    //TODO: deallocate out_string before exiting

    exit(1);
  }

  return 0;
}

int read_int(int *out_int){
  char *input = NULL;
  int rc = -100;
  rc = read_string(&input, MAX_DATA);
  if(rc==-100){
    printf("Failed to allocate memory");
    exit(1);
  }

  *out_int = atoi(input);

  free(input);
  return 0;
}

int read_scan(const char *fmt, ...){
  int i = 0;
  int rc = 0;
  int *out_int = NULL;
  char *out_char = NULL;
  char **out_string = NULL;
  int max_buffer = 0;

  va_list argp;
  va_start(argp,fmt);

  for (i = 0; fmt[i] != '\0'; i++) {
    if(fmt[i] == '%'){
      i++;
      switch (fmt[i]) {
        case '\0':
          //sentinel
          break;
        case 'd':
          out_int = va_arg(argp,int *);
          rc = read_int(out_int);
          //TODO: check
          break;
        case 'c':
          out_char = va_arg(argp,char *);
          *out_char = fgetc(stdin);
          //TODO: check
          break;
        case 's':
          max_buffer = va_arg(argp,int);
          out_string = va_arg(argp,char **);
          rc = read_string(out_string,max_buffer);
          //TODO: check
          break;
        default:
          ;//TODO: sentinel
      }
    } else{
      fgetc(stdin);
    }
    //TODO: check
    //check(!feof(stdin) && !ferror(stdin), "Input error.");
  }
  va_end(argp);
  return 0;
}

int main(int argc, char *argv[]){
  char * first_name = NULL;
  char initial = ' ';
  char *last_name = NULL;
  int age = 0;

  printf("What's your First Name? ");
  int rc = read_scan("%s", MAX_DATA, &first_name);
  /*if(!rc){
    printf("Failed to read");
    exit(1);
  }*/

  printf("What's your initial? ");
  rc = read_scan("%c\n", &initial);
  //printf("rc is %c \n",rc);
  /*if(!rc){
    printf("Failed to read");
    exit(1);
  }*/

  printf("What's your Last Name? ");
  rc = read_scan("%s", MAX_DATA, &last_name);
  /*if(!rc){
    printf("Failed to read");
    exit(1);
  }*/

  printf("How old are you? ");
  rc = read_scan("%d", &age);
  /*if(!rc){
    printf("Failed to read");
    exit(1);
  }*/

  printf("---------RESULTS---------\n");
  printf("First Name: %s\n", first_name);
  printf("Initial: %c\n", initial);
  printf("Last Name: %s\n", last_name);
  printf("Age: %d\n", age);

  free(first_name);
  free(last_name);

  return 0;
}
